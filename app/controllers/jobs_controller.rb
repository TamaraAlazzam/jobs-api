class JobsController < ApplicationController
  before_action :find_job, only: [:show, :update, :destroy]
  before_action :authenticate_user!
  load_and_authorize_resource

  # GET job
  # GET jobs.json
  def index
    @jobs = Job.reorder("created_at DESC").paginate(page: params[:page], per_page: 10)
    render json: @jobs
  end

  # GET job/1
  # GET job/1.json
  def show
    render json: @job, status: :ok
  end

  # POST job
  # POST job.json
  def create
    @job = Job.new(job_params)

    if @job.save
      render json: @job, status: :created
    else
      render json: @job.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT job/1
  # PATCH/PUT job/1.json
  def update
    if @job.update(job_params)
      render json: @job, status: :ok
    else
      render json: @job.errors, status: :unprocessable_entity
    end
  end

  # DELETE job/1
  # DELETE job/1.json
  def destroy
    @job.destroy
  end

  private

  def job_params
    params.require(:job).permit(:title, :description)
  end

  def find_job
    @job = Job.find(params[:id])
  end
end
