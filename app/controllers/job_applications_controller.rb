class JobApplicationsController < ApplicationController
  before_action :find_job_application, only: [:show, :update, :destroy]
  before_action :authenticate_user!
  load_and_authorize_resource

  # GET job_applications
  # GET job_applications.json
  def index
    @jobApplications = JobApplication.reorder("created_at DESC").paginate(page: params[:page], per_page: 10)
    render json: @jobApplications
  end

  # GET job_applications/1
  # GET job_applications/1.json
  def show
    render json: @jobApplication, status: :ok
    @jobApplication.update_attribute(:seen, true)
  end

  # POST job_applications
  # POST job_applications.json
  def create
    if current_user.job_applications.find_by(job_id: params["job_id"])
      render json: "You have already applied to this job!!", status: :ok
    else
      @jobApplication = current_user.job_applications.new(job_application_params)

      if @jobApplication.save
        render json: @jobApplication, status: :created
      else
        render json: @jobApplication.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT job_applications/1
  # PATCH/PUT job_applications/1.json
  def update
    if @jobApplication.update(job_application_params)
      render json: @jobApplication, status: :ok
    else
      render json: @jobApplication.errors, status: :unprocessable_entity
    end
  end

  # DELETE job_applications/1
  # DELETE job_applications/1.json
  def destroy
    @jobApplication.destroy
  end

  private

  def job_application_params
    params.permit(:job_id)
  end

  def find_job_application
    @jobApplication = JobApplication.find(params[:id])
  end
end
