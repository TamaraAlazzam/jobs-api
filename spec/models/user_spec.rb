require "rails_helper"

RSpec.describe User, type: :model do
  describe "validation tests" do
    it "ensures password presence" do
      user = User.new(email: "test@mail.com").save
      expect(user).to eq(false)
    end

    it "ensures email presence" do
      user = User.new(password: "password").save
      expect(user).to eq(false)
    end

    it "should save successfully" do
      @user = FactoryBot.build(:user)
      saved_user = User.new(email: @user.email, password: @user.password).save
      expect(saved_user).to eq(true)
    end
  end
end
