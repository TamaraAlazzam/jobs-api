require "rails_helper"

RSpec.describe JobApplication, type: :model do
  it "ensures job_id presence" do
    job_application = JobApplication.new(user_id: 1).save
    expect(job_application).to eq(false)
  end

  it "ensures user_id presence" do
    job_application = JobApplication.new(job_id: 1).save
    expect(job_application).to eq(false)
  end
end
