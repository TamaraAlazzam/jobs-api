require "rails_helper"

RSpec.describe Job, type: :model do
  it "ensures title presence" do
    job = Job.new(description: "Full-Time").save
    expect(job).to eq(false)
  end

  it "ensures description presence" do
    job = Job.new(title: "dev").save
    expect(job).to eq(false)
  end

  it "should save successfully" do
    job = Job.new(title: "dev", description: "Full-Time").save
    expect(job).to eq(true)
  end
end
