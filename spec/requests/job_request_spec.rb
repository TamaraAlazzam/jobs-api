require "rails_helper"

RSpec.describe JobsController, type: :request do
  describe "Normal User" do
    before(:each) do
      @user = FactoryBot.create(:user)

      @request_param = {
        email: @user.email,
        password: @user.password,
      }

      # user_session_path eq => "/auth/sign_in"
      post user_session_path, params: @request_param, as: :json

      @credentials = {
        "uid" => response.headers["uid"],
        "client" => response.headers["client"],
        "access-token" => response.headers["access-token"],
      }

      @job = FactoryBot.create(:job)
    end

    it "Get all jobs successfully" do
      get jobs_path, headers: @credentials, params: { "page" => 1 }

      expect(response).to have_http_status(:ok)

      result = JSON.parse(response.body)

      expect(result).to eq Job.reorder("created_at DESC").paginate(page: 1, per_page: 10).as_json
    end

    it "Create job should return unauthorized" do
      post jobs_path, params: { job: { title: @job.title, description: @job.description } }

      expect(response).to have_http_status(:unauthorized)
    end

    it "Update a job should return unauthorized" do
      patch job_path(@job.id), params: { job: { title: @job.title, description: @job.description } }
      expect(response).to have_http_status(:unauthorized)
    end

    it "Destroy a job should return unauthorized" do
      delete job_path(@job.id)
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "Admin" do
    before(:each) do
      @user = FactoryBot.create(:user, admin: true)

      @request_param = {
        email: @user.email,
        password: @user.password,
      }

      # user_session_path eq => "/auth/sign_in"
      post user_session_path, params: @request_param, as: :json

      @credentials = {
        "uid" => response.headers["uid"],
        "client" => response.headers["client"],
        "access-token" => response.headers["access-token"],
      }

      @job = FactoryBot.create(:job)
    end

    it "Create a job successfully" do
      post jobs_path, headers: @credentials, params: { job: { title: @job.title, description: @job.description } }

      expect(response).to have_http_status(:created)
    end

    it "Update a job successfully" do
      patch job_path(@job.id), params: { job: { title: "updated" } }, headers: @credentials
      expect(response).to have_http_status(200)
    end
  end
end
