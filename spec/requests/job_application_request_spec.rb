require "rails_helper"

RSpec.describe JobApplicationsController, type: :request do
  describe "Admin" do
    before(:each) do
      @user = FactoryBot.create(:user, admin: true)

      @params = {
        email: @user.email,
        password: @user.password,
      }
      @job = FactoryBot.create(:job)
      FactoryBot.create(:job_application, job_id: @job.id, user_id: @user.id)

      post user_session_path, params: @params, as: :json

      # user_session_path eq => "/auth/sign_in"
      @credentials = {
        "uid" => response.headers["uid"],
        "client" => response.headers["client"],
        "access-token" => response.headers["access-token"],
      }
    end

    it "Get all successfully" do
      get job_applications_path, params: { "page" => 1 }, headers: @credentials

      expect(response).to have_http_status(:ok)

      result = JSON.parse(response.body)

      expect(result).to eq JobApplication.reorder("created_at DESC").paginate(page: 1, per_page: 10).as_json
    end
  end

  describe "Normal user" do
    before(:each) do
      @user = FactoryBot.create(:user)

      @params = {
        email: @user.email,
        password: @user.password,
      }
      @job = FactoryBot.create(:job)
      FactoryBot.create(:job_application, job_id: @job.id, user_id: @user.id)

      post user_session_path, params: @params, as: :json

      # user_session_path eq => "/auth/sign_in"
      @credentials = {
        "uid" => response.headers["uid"],
        "client" => response.headers["client"],
        "access-token" => response.headers["access-token"],
      }

      @job = FactoryBot.create(:job)
      @job_application = FactoryBot.build(:job_application, user_id: @user.id, job_id: @job.id)
    end

    it "Get all should return unauthorized" do
      get job_applications_path, params: { "page" => 1 }
      expect(response).to have_http_status(:unauthorized)
    end

    it "Create job application successfully" do
      post job_applications_path, params: { job_application: { user_id: @user.id, job_id: @job.id } }
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
