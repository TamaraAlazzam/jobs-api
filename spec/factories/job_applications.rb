FactoryBot.define do
  factory :job_application do
    seen { false }
    job_id { 1 }
    user_id { 1 }
  end
end
